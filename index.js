const puppeteer = require("puppeteer");

(async () => {
 
	const browser = await puppeteer.launch({
		args: ['--no-sandbox','--ignore-certificate-errors'],
		handles: true
	});
	const page = await browser.newPage();
	

	await page.goto('http://localhost:32768/d/4sOhsvdGz/poc-prints-2?orgId=1');
	
	await page.waitFor(1000);
	
	//Login 
	
	// Username
	await page.evaluate(() => document.getElementsByName("user")[0].value = "admin");

	// Password
	await page.evaluate(() => document.getElementsByName("password")[0].value = "12345");
	

	await page.waitFor(1000);

	await page.evaluate(() => document.querySelector("button").click());

	await page.waitFor(2000);
	
	await page.goto("http://localhost:32768/d/4sOhsvdGz/poc-prints-2?orgId=1&kiosk")

	await page.waitFor(6000);

	await page.screenshot({ path : 'teste.png'})
	
	await browser.close();


})();
